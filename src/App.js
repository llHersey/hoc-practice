import React from "react";
import Component from "./Component";

function App(props) {
  const input = [{ loan_id: 1000, description: "Loan Description" }, { loan_amount: 50000, description: "Loan Amount" }];

  return <Component input={input} />;
}

export default App;
