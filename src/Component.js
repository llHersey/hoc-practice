import React from "react";
import { HOCWithHooks, HOCWithClass } from "./HOC";

function Component(props) {
  const { input, filter, fonts } = props;

  const mapped = input.reduce((prev, value) => {
    const { description, ...rest } = value;
    const key = Object.keys(rest)[0];

    if (filter && key !== filter) {
      return { ...prev };
    }

    return {
      ...prev,
      [key]: {
        value: value[key],
        description,
      },
    };
  }, {});

  return (
    <div className="App">
      <ul>
        {Object.keys(mapped).map(key => {
          return <li key={key}>{`key: ${key}. Value: ${mapped[key].value}. Description: ${mapped[key].description}`}</li>;
        })}
      </ul>
      <ul>
        {fonts.map(font => {
          return <li key={font.family}>{font.family}</li>;
        })}
      </ul>
    </div>
  );
}

export default HOCWithHooks(Component);
