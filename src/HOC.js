import React, { useEffect, useState, Component as ReactComponent } from "react";

function HOCWithHooks(Component) {
  return props => {
    const [fonts, setFonts] = useState([]);

    useEffect(() => {
      fetch("https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBev35BMvf_aDA7FI6mHOk7KDgOBXncZW8")
        .then(res => res.json())
        .then(fonts => fonts.items.slice(fonts.items.length - 10))
        .then(filteredFonts => setFonts(filteredFonts));
    }, []);

    return <Component fonts={fonts} {...props} />;
  };
}

function HOCWithClass(Component) {
  return class extends ReactComponent {
    state = {
      fonts: [],
    };

    componentDidMount() {
      fetch("https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBev35BMvf_aDA7FI6mHOk7KDgOBXncZW8")
        .then(res => res.json())
        .then(fonts => fonts.items.slice(fonts.items.length - 10))
        .then(filteredFonts => this.setState({ fonts: filteredFonts }));
    }

    render() {
      return <Component fonts={this.state.fonts} {...this.props} />;
    }
  };
}

export { HOCWithHooks, HOCWithClass };
